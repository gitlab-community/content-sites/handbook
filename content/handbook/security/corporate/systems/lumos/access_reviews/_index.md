---
title: Lumos Access Reviews Guide
---

## Overview

Lumos is our access review platform. If you are a DRI for an app, technical owner, or business owner, you may be assigned access review tasks from Lumos.

## Access Review Walkthrough

When a review is assigned to you, you will receive a notification through Slack or Email.

1. Go to your [access reviews](https://app.lumosidentity.com/access_reviews) in Lumos

    <img src="/images/security/corporate/systems/lumos/access_reviews/access_review_1.png" alt="Click Review Accounts in the notification" width="600"/><br>

2. Click "Continue Review" for an app to start a review

    <img src="/images/security/corporate/systems/lumos/access_reviews/access_review_2.png" alt="On the app list, click Continue review to the far-right" width="600"/><br>

3. Review the line items assigned to you

    <img src="/images/security/corporate/systems/lumos/access_reviews/access_review_3.png" alt="Review the users assigned to you" width="600"/><br>

4. Approve or reject the accounts assigned to you

    <img src="/images/security/corporate/systems/lumos/access_reviews/access_review_4.png" alt="Approve, reject, or modify access in bulk" width="600"/><br>

- Add a note for any line item to add context for auditors

    <img src="/images/security/corporate/systems/lumos/access_reviews/access_review_note.png" alt="Click the + to add a note" width="600"/><br>
