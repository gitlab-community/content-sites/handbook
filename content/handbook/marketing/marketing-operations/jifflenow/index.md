---
title: JiffleNow
description: Automated appointment scheduling of in-person, and virtual B2B meetings at events or campaigns.
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## Overview

JiffleNow is an automated appointment scheduling of in-person, and virtual B2B meetings at events or campaigns.

## Current Use

To following along on the project, follow Asana project: https://app.asana.com/0/1208791785314172/1208791670565152

## Access

Jifflenow will be made available to Sales attending events, EBAs and event managers. If you need access, please create an [AR](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request). Access for Meeting Requestors via SFDC is granted via the JiffleNow Permission set, so please make sure the Sales Systems team is tagged (Similar to SFDC access requests).

## User Roles

- **Requestor** - Anyone that has access to schedule meetings within the tool.
- **Meeting Manger** - Has the ability to manage all meetings for an event.

## Training

We have put together a comprehensive [training slide deck](https://docs.google.com/presentation/d/1yzGICzWa1687_da4tte7rV3qnMbwtZjEVpgTDb3XjAA/edit#slide=id.g1d24c3e4ddd_5_252), and we also have linked training videos below:

- [EBA and Meeting Manager Training](https://drive.google.com/file/d/1MlgZaD-Z41DSaVfAAql4uR5gIIz4velv/view?usp=drive_link)
- [Meeting Requestor Training - for Sales and Field](https://drive.google.com/file/d/1JXDmdWs2391CW4h7C3Q2rX8I4NNJPYZv/view?usp=sharing)
- Managing On-Site Meetings
- Mobile App (to be created)

## Salesforce.com Integration

This connection will allow for Sales to book meetings directly within Salesforce.com and allow JiffleNow to access and link contact records. Meeting requestors can access JiffleNow to schedule meetings through a button on the Contact, Account and Opportunity record.

## Support

Reach out in #proj_jifflenow-implementation for more information.

For event specfic help, go to:

- #googlenext-execmtgs-2025
- #rsa-conference-execmtgs-2025
